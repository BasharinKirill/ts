import java.io.Serializable;
import java.util.Objects;

public final class Student implements Serializable{
    private final String name;
    private final String task;

    public Student(String name, String task) {
        this.name = name;
        this.task = task;
    }


    public String getName() {
        return name;
    }


    public String getTask() {
        return task;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(name, student.name) &&
                Objects.equals(task, student.task);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, task);
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", task='" + task + '\'' +
                '}';
    }

}
