import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
        LocalDate localDate = LocalDate.now();
        Scanner scanner = new Scanner(System.in);
        WorkWhithFile workWhithFile = new WorkWhithFile();
        ConsoleInput consoleInput = new ConsoleInput(scanner, workWhithFile);
        consoleInput.workWhithStartMenu();
    }
    // TODO: 11.01.2018 написать тесты на весь ГК))
}
