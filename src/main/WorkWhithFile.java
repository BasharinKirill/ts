import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class WorkWhithFile {
    private Path path = Paths.get("calendar.csv");
    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");

    // TODO: 12.01.2018 после написания view избавиться от постоянного перезаписывания файла
    public void addStudentAtDay(Map<LocalDate, Student> taskMap, LocalDate localDate, Student student) {
        while (!localDate.equals(localDate.plusMonths(3))) {
            int i = 0;
            if (localDate.getDayOfWeek().equals(DayOfWeek.MONDAY)
                    || localDate.getDayOfWeek().equals(DayOfWeek.TUESDAY)
                    || localDate.getDayOfWeek().equals(DayOfWeek.WEDNESDAY)
                    || localDate.getDayOfWeek().equals(DayOfWeek.THURSDAY)
                    || localDate.getDayOfWeek().equals(DayOfWeek.FRIDAY)
                    || localDate.getDayOfWeek().equals(DayOfWeek.SATURDAY)
                    || localDate.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
                while (i < 10) {
                    taskMap.put(localDate, student);
                    localDate = localDate.plusDays(7);
                    i++;
                }
                break;
            }
        }
    }

    public void serializationFile(Map<LocalDate, Student> taskMap) {
        try(FileOutputStream fos =
                    new FileOutputStream("calendar.csv");
            ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(taskMap);
        } catch (IOException e) {
            System.out.println("Fie not founding");
        }
    }

    public Map<LocalDate, Student> deserializationFile() throws IOException {
        Map<LocalDate, Student> taskMap= new HashMap<>();
        try (FileInputStream fileInputStream = new FileInputStream("calendar.csv");
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
            taskMap = (Map<LocalDate, Student>) objectInputStream.readObject();
        } catch (FileNotFoundException e) {
            System.out.println("File not founding");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return taskMap;
    }
}
