import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ConsoleInput {
    private Scanner scanner = new Scanner(System.in);
    // TODO: 11.01.2018 написать view
    private WorkWhithFile workWhithFile;
    private Map<LocalDate, Student> taskMap = new HashMap<>();

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");

    public ConsoleInput(Scanner scanner, WorkWhithFile workWhithFile) {
        this.scanner = scanner;
        this.workWhithFile = workWhithFile;
    }

    public void printMenu() {
        System.out.println("Это твоё расписание уроков");
        System.out.println("Для ыхода нажми #");
        System.out.println("Назначить расписание для ученика. Нажми 1");
        System.out.println("Удалить расписание для ученика. Нажми 2");
        System.out.println("Перенести дату для ученика. Нажми 3");
    }

    public void workWhithStartMenu() throws IOException {
        printMenu();
        taskMap = new HashMap<>(workWhithFile.deserializationFile());
        System.out.println(taskMap);
        String input = scanner.nextLine();
        switch (input) {
            case "#":
                return;
            case "1":
                writeStudent();
                break;
            case "2":
                deleteStudentAtDay(taskMap);
                break;
            case "3":
                return;
        }
        workWhithFile.serializationFile(taskMap);
        System.out.println("То что пишется: " + taskMap);
        return;
    }


    public void writeStudent() {
        System.out.println("Для выхода нажми #");
        LocalDate localDate = null;
        String name;
        while (true) {
            System.out.println("Введи имя студента");
            name = scanner.nextLine();
            if (name.equals("#")) {
                return;
            }
            break;
        }
        String task;
        while (true) {
            System.out.println("Введи название урока");
            task = scanner.nextLine();
            if (task.equals("#")) {
                return;
            }
            break;
        }
        String data;
        while (true) {
            System.out.println("Введи дату в формате yyyy-MMM-dd");
            // TODO: 25.01.2018 В случае неправильного ввода даты дробит введенные данные на отдельные числа и в зависимости от количества пробегает по if и sout
            data = scanner.next();
            if (data.equals("#")) {
                return;
            }
            try {
                localDate = LocalDate.parse(data);
            } catch (DateTimeParseException | NullPointerException e) {
                System.out.println("Введите корректную дату или нажмите для выхода #");
                continue;
            }
            break;
        }
        Student student = new Student(name, task);
        workWhithFile.addStudentAtDay(taskMap, localDate, student);
    }

    public void deleteStudentAtDay(Map<LocalDate, Student> taskMap) {
        System.out.println("Введи дату в формате yyyy-MMM-dd для удаления или нажмите для выхода #");
        LocalDate localDate;
        String data;
        while (true) {
            data = scanner.next();
            if (data.equals("#")) {
                return;
            }
            try {
                localDate = LocalDate.parse(data);
            } catch (DateTimeParseException | NullPointerException e) {
                System.out.println("Введите корректную дату или нажмите для выхода #");
                continue;
            }
            break;
        }
        taskMap.remove(localDate);
    }
    // TODO: 15.01.2018 написать методы для определения клиента и даты!
}
